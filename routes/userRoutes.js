const express = require('express')
const router = express.Router()
const userController=require('../controllers/userControllers')
const productController=require('../controllers/productControllers')
const auth = require('../auth')

// user registration
router.post('/register',(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

// user authentication
router.post('/authenticate',(req,res)=>{
	userController.authenticateUser(req.body).then(resultFromController=>res.send(resultFromController))
})

// get all user
router.get('/all',(req,res)=>{
	userController.getAllUser().then(resultFromController=>res.send(resultFromController))
})

// get all orders
router.get('/orders',auth.verify,(req,res)=>{
	let userData=auth.decode(req.headers.authorization)
	if(userData.isAdmin){
	userController.getAllOrders().then(resultFromController=>res.send(resultFromController))
	} else {
		res.send('Not an Admin')
	}
})

// get specific order
router.get('/myOrders',auth.verify,(req,res)=>{
	let data={
		userId:auth.decode(req.headers.authorization).id,
	}
	let admin = auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
	userController.getOrder(data).then(resultFromController=>res.send(resultFromController))
	} else{
		res.send(`You're an Admin`)
	}
}) 

// retrieve specific user
router.get('/details',auth.verify,(req,res)=>{

	const userData=auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getUser({userId:userData.id}).then(resultFromController=>{
		if(resultFromController==false){
			res.send("user does not exist")
		} else{
		res.send(resultFromController) 
		}
	})
})

// set user as admin
router.put('/:userId/setAsAdmin',auth.verify,(req,res)=>{
	let userData=auth.decode(req.headers.authorization)
	if (userData.isAdmin){
		userController.userToAdmin(req.params).then(resultFromController=>res.send(resultFromController))
	} else {
		res.send('Not an Admin')
	}
})

// create order
router.post('/addToCart',auth.verify,(req,res)=>{
	let data = {
		userId:auth.decode(req.headers.authorization).id,
		productId:req.body.productId,
		quantity:req.body.quantity
	}
	let admin=auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
		userController.createCart(data).then(resultFromController=>res.send(resultFromController))
	} else {
		return res.send('User is an admin')
	}
})

// delete all orders of specific user
router.delete('/myOrders/delete',auth.verify,(req,res)=>{
	let data={
		userId:auth.decode(req.headers.authorization).id,
	}
	let admin = auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
	userController.deleteAllOrder(data).then(resultFromController=>res.send(resultFromController))
	} else{
		res.send(`You're an Admin`)
	}
})

// delete specific product/order of user
router.delete('/getCart/deleteProduct',auth.verify,(req,res)=>{
	let data ={
		userId:auth.decode(req.headers.authorization).id,
		productId:req.body.productId
	}
	let admin=auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
		userController.deleteProduct(data).then(resultFromController=>res.send(resultFromController))
	} else {
		return res.send('User is an admin')
	}
})

router.get('/getCart',auth.verify,(req,res)=>{
	let data ={
		userId:auth.decode(req.headers.authorization).id
	}

	let admin=auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
		userController.getCart(data).then(resultFromController=>res.send(resultFromController))
	} else {
		return res.send('User is an admin')
	}
})

router.post('/checkout',auth.verify,(req,res)=>{
	let data = {
		userId:auth.decode(req.headers.authorization).id
	}
	let admin=auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
		userController.createOrder(data).then(resultFromController=>res.send(resultFromController))
	} else {
		return res.send('User is an admin')
	}
})

router.delete('/getCart/removeProduct',auth.verify,(req,res)=>{
	let data ={
		userId:auth.decode(req.headers.authorization).id,
		productId:req.body.productId
	}
	let admin=auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
		userController.removeFromCart(data).then(resultFromController=>res.send(resultFromController))
	} else {
		return res.send('User is an admin')
	}
})



module.exports=router