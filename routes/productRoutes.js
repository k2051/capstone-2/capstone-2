const express = require('express')
const Product = require('../models/Product')
const router = express.Router()
const userController=require('../controllers/userControllers')
const productController=require('../controllers/productControllers')
const auth = require('../auth')

// retrieve all products
router.get('/',(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
})

// retrieve all active products
router.get('/active',(req,res)=>{
	productController.getAllActive().then(resultFromController=>res.send(resultFromController))
})


// retrieve specific product
router.get('/:productId',(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
})

// create product
router.post('/',auth.verify,(req,res)=>{
	let userData=auth.decode(req.headers.authorization)
	if (userData.isAdmin){
		productController.createProduct(req.body).then(resultFromController=>res.send(resultFromController))
	} else {
		res.send('Not an Admin')
	}
})

// update product
router.put('/:productId',auth.verify,(req,res)=>{
	let data={
		productId:req.params.productId,
		isAdmin:auth.decode(req.headers.authorization).isAdmin,
		updatedProduct:req.body,
		userId:auth.decode(req.headers.authorization).id
	}
	if (data.isAdmin){
	productController.updateProduct(data).then(resultFromController=>res.send(resultFromController))
	} else {
		res.send('Not an admin')
	}
})

// archive product
router.put('/:productId/archive',auth.verify,(req,res)=>{
	const data={
		productId:req.params.productId,
		isAdmin:auth.decode(req.headers.authorization).isAdmin,
		userId:auth.decode(req.headers.authorization).id
	}
	if (data.isAdmin){
	productController.archiveProduct(data).then(resultFromController=>res.send(resultFromController))
	} else {
		res.send('Not an admin')
	}
})


module.exports=router