const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const cartSchema = new mongoose.Schema(
  {
    products:[{
      productId:String,
      name:String,
      quantity:{
        type:Number,
        default:1
      },
      subtotal:Number,
      
  }],
    cartTotal: Number,
    orderdBy: { type: ObjectId, ref: "User" },
  }
);

module.exports = mongoose.model("Cart", cartSchema);