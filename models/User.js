const mongoose = require('mongoose')
const {ObjectId}=  mongoose.Schema


const userSchema = new mongoose.Schema({
	email:{
		type:String,
		required:[true,'E-mail is required']
	},
	password:{
		type:String,
		required:[true,'Password is required']
	},
	isAdmin:{
		type:Boolean,
		default:false
	},
	access:String,
	cart:[
		{
			productId:{
				type:String,
				required:[true,'Product ID is required']
			},
			quantity:{
				type:Number,
				default:1
			}
			// purchasedOn:{
			// 	type:Date,
			// 	default:new Date()
			// },
			// isActive:{
			// 	type:Boolean,
			// 	default:true
			// }
		}
	]
})

module.exports=mongoose.model('User', userSchema)