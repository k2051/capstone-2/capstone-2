const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const orderSchema = new mongoose.Schema({
	// userId:{
	// 	type:String,
	// 	required:[true]
	// },
	products:[{
		productId:String,
		name:String,
		quantity:{
			type:Number,
			default:1
		},
		subtotal:Number,
		isActive:{
			type:Boolean,
			default:true
		}
	}
	],
	totalAmount:Number,
	purchasedOn:{
		type:Date,
		default: new Date()
	},
	orderdBy:{ type: ObjectId, ref: "User" }

})
module.exports=mongoose.model('Order', orderSchema)