const User = require('../models/User')
const Product = require('../models/Product')
const Order = require ('../models/Order')
const bcrypt=require('bcrypt')
const auth = require('../auth')

// get all products
module.exports.getAllProducts=()=>{
	return Product.find({}).then(result=>result)
}

// get all active products
module.exports.getAllActive=()=>{
	return Product.find({isActive:true}).then(result=>result)
}

// get specific product
module.exports.getProduct=(reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>result)
}

// create product
module.exports.createProduct=(reqBody)=>{
	let newProduct = new Product({
		name:reqBody.name,
		description:reqBody.description,
		price:reqBody.price,
		isActive:reqBody.isActive,
		createdOn:reqBody.createdOn
	})

	return newProduct.save().then((product,error)=>{
		if (error){
			return false
		} else {
			return product
		}
	})
}

// update product
module.exports.updateProduct=async (data)=>{

	let updateProduct=await Product.findById(data.productId).then((result,error)=>{
		const beforeUpdate={
			name:result.name,
			description:result.description,
			price:result.price,
			isActive:result.isActive
		}
			if(data.updatedProduct.name==null){
				result.name=beforeUpdate.name
			} else {
				result.name=data.updatedProduct.name
			}

			if (data.updatedProduct.description==null){
				result.description=beforeUpdate.description
			} else {
				result.description=data.updatedProduct.description
			}
			
			if (data.updatedProduct.price==null){
				result.price=beforeUpdate.price
			} else {
				result.price=data.updatedProduct.price
			}
			if(data.updatedProduct.isActive==null){
				result.isActive=beforeUpdate.isActive
			} else{
				result.isActive=data.updatedProduct.isActive
			}	
			
			

			return result.save().then((updatedProduct,error)=>{
				if (error){
					return false
				} else{
					return true
				}
			})
		 
	})
	return updateProduct
	// console.log(updateProduct)


	/*let updateUser=await User.updateMany({"orders.productId":data.productId},{$set:{"orders.$.isActive":updateProduct.isActive}}).then((result,err)=>{
		if(err){
			return false
		} else {
			return true
		}
	})
	// console.log(updateUser)

	let updateOrder = await Order.updateMany({"products.productId":data.productId},{$set:{"products.$.isActive":updateProduct.isActive}}).then((result,err)=>{
		if(err){
			return false
		} else {
			return true
		}
	})
	// console.log(updateOrder)

	let totalAmount=await Order.find({"orders.productId":data.productId}).then(order=>{
		
		for (let i=0;i<order.length;i++){
			let orders=order[i].products
			
			let sum=0
			for(a=0;a<orders.length;a++){
				if(orders[a].productId==data.productId){
					let newTotal=updateProduct.price*orders[a].quantity
					orders[a].total=newTotal
					console.log(orders[a].total)
				} 
				if(orders[a].isActive==true){
					
					sum += orders[a].total
				} 
			order[i].totalAmount=sum
			
			// console.log(order[i].totalAmount)
			}

			 order[i].save()
		}

	})

		if(updateProduct!=null && updateOrder && updateUser){
			return `${updateProduct.name} has been updated`
		} else {
			return false
		}*/
}
	

// archive product
module.exports.archiveProduct=async (data)=>{
	let archiveProduct= await Product.findById(data.productId).then((result)=>{
		
			result.isActive=false
			return result.save().then((archivedProduct,error)=>{
				if (error){
					return false
				} else {
					return result
				}
			})
		
	})
	// console.log(archiveProduct)

	/*let archiveUserOrder= await User.updateMany({"orders.productId":data.productId},{$set:{"orders.$.isActive":false}}).then((result,err)=>{
		if(err){
			return false
		} else {
			return true
		}
	})
	// console.log(archiveUserOrder)

	let archivedOrder=await Order.updateMany({"products.productId":data.productId},{$set:{"products.$.isActive":false}}).then((result,err)=>{
		if(err){
			return false
		} else {
			return true
		}
	})
	// console.log(archivedOrder)
	let totalAmount=await Order.find({"orders.productId":data.productId}).then(order=>{
		for (let i=0;i<order.length;i++){
			let orders=order[i].products
			// console.log(orders)
			let sum=0
			for(a=0;a<orders.length;a++){
				if(orders[a].isActive==true){
				sum += orders[a].total
				} 
			order[i].totalAmount=sum
			// console.log(order[i].totalAmount)
			}

			order[i].save()
		}
	})


	if(archiveProduct || (archiveUserOrder && archivedOrder)){
		return `Product Archived`
	} else{
		return false
	} */
	return archiveProduct
}