const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const Cart = require('../models/Cart')
const bcrypt=require('bcrypt')
const auth = require('../auth')

// user registration control
module.exports.registerUser=(reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if (result==null){
			let newUser=new User({
					email:reqBody.email,
					password:bcrypt.hashSync(reqBody.password,10),
					isAdmin:reqBody.isAdmin
				})
				return newUser.save().then((user,error)=>{
					if (error){
						return false
					} else {
						return true
					}
				})
		} else {
			return false
		}
	})
}

// user authentication
module.exports.authenticateUser=(reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return `Invalid Email or Password`
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)
			if (isPasswordCorrect) {
				return{access:auth.createAccessToken(result)}
			} else{
				return false
			}

		}
	})
}

// get all users
module.exports.getAllUser=()=>{
	return User.find({}).then(result=>result)
}

// get specific user
module.exports.getUser=(reqBody)=>{
	return User.findById(reqBody.userId).then(result=>{
		if(result==null){
			return false
		} else{
			// result.password=''
			return result
		}
	})
}

// set user to admin
module.exports.userToAdmin=(reqParams)=>{
	return User.findById(reqParams.userId).then((result,error)=>{
		result.isAdmin=true
		return result.save().then((setToAdmin,error)=>{
			if(error){
				return false
			} else {
				return `${setToAdmin.email} is now an admin`
			}
		})
	})
}

// create cart
module.exports.createCart= async (data)=>{

	let amount= await Product.findById(data.productId).then( (p,error)=>{
			if (error){
				return false
			} else{
				return price=p.price
			}

			})

	let isProductActive = await Product.findById(data.productId).then(result=>{
		return result.isActive
	})

	let productName = await Product.findById(data.productId).then(result=>{
		return result.name
	})
	// console.log(productName)
	// push to user model
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		let test=user.cart.find(id=>id.productId==data.productId)
		// test if user has no orders on that product yet
		// if none push to orders array in User model
		// if yes, increment quantity element in orders array
			if(test==undefined){
				if(isProductActive){
						user.cart.push({
							productId:data.productId,
							name:productName,
							quantity:data.quantity})
				} 
			} else{

				if(isProductActive){

				test.quantity+=data.quantity
				
				} 	
			}
			
			// console.log(user.cart)
			return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return true
					}
				})

	})
	
	// push to Cart Model
	let isCartUpdated=await Cart.findOne({orderdBy:data.userId}).then(cart=>{
		let result=cart.products.find(id=>id.productId==data.productId)
		if(cart==null){
			if(isProductActive){
				let newCart= new Cart({
					
					products:{
						productId:data.productId,
						name: productName,
						quantity:data.quantity,
						subtotal:amount*data.quantity
						
					},
					cartTotal:amount*data.quantity,
					orderdBy:data.userId
				})
			 return newCart.save().then((order,error)=>{
					if (error){
						return false
					} else{
						return true
					}
				})
		} else {
				return
			}
	} else { 
			let test=cart.products.find(id=>id.productId==data.productId)
			
			console.log(test)
			if(test==undefined){
				if(isProductActive){
					cart.products.push({
					productId:data.productId,
					name: productName,
					quantity:data.quantity,
					subtotal:amount*data.quantity	
					})
			} else {
				return
			}
			} else {
				if (isProductActive){
					test.quantity+=data.quantity
					
					test.subtotal=test.quantity*amount
			} else {
				return
			}
			}			
		}
		let sum = 0
			for(let i=0;i<cart.products.length;i++){
					sum+=cart.products[i].subtotal
			}
			cart.cartTotal=sum

		return cart.save().then((order,error)=>{
					if(error){
						return false
					} else {
						return result
					}
				})
		
	})	

		
	return isCartUpdated
	
	
}

// get all orders
module.exports.getAllOrders=()=>{
	return Order.find({}).then(order=>{
		if (order.length>0){
			return order
		} else {
			return false
		}
	})
}


// get specific order
module.exports.getOrder=(data)=>{
return userOrder= Order.find({orderdBy:data.userId}).then(order=>{
	
		if (order==null || order==' ' ||order==undefined){
			return `You don't have any orders yet!`
		} else {
			return order

		}
		
		
		

	})
}

// delete all cart items

module.exports.deleteAllOrder= async (data)=>{
	let cart= await Cart.findOneAndDelete({orderdBy:data.userId}).then(result=>{
		if (result==null || result==' ' ||result==undefined){
			return `You don't have any items yet!`
		} else {
			return true
		}
	})
	// console.log (order)

	let cartUser = await User.findById(data.userId).then(user=>{
		user.cart.splice(0,user.cart.length)
		return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return user
					}
				})
	})
	// console.log(orderUser)

	if(cartUser && cart){
		return `Order has been deleted`
	} else{
		return false
	}
}

// delete product/product quantity in order
module.exports.deleteProduct= async (data)=>{
	let amount= await Product.findById(data.productId).then( (p,error)=>{
			if (error){
				return false
			} else{
				return price=p.price
			}

			})
	let isUserUpdated= await User.findById(data.userId).then(user=>{
		let test=user.cart.find(id=>id.productId==data.productId)
		let index = user.cart.findIndex(i=>i.productId==data.productId)
		
		
			if(test==undefined){
				return false
			} else{
				test.quantity--
				if (test.quantity<=0){
					user.cart.splice(index,1)
				} 
				return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return test
					}
				})
			}		
	})
	// console.log(isUserUpdated)
	let isCartUpdated= await Cart.findOne({orderdBy:data.userId}).then(cart=>{
		let test=cart.products.find(id=>id.productId==data.productId)
		let index = cart.products.findIndex(i=>i.productId==data.productId)
		// console.log(order)
			if (test===undefined){
				return false
			} else {
				test.quantity--
				test.subtotal=test.quantity*amount
				if (test.quantity<=0){
					cart.products.splice(index,1)
				} 
				
				return cart.save().then((order,error)=>{
					if(error){
						return false
					} else {
						// let product=order.products.find(id=>id.productId==data.productId)
						return test
					}
				})
			}
	})
	let cartTotal=await Cart.findOne({orderdBy:data.userId}).then(cart=>{
		// console.log(cart)
		// console.log(cart.length)
		let sum = 0
			for(let i=0;i<cart.products.length;i++){
					sum+=cart.products[i].subtotal
			}
			cart.cartTotal=sum
		// console.log(sum)
		cart.save()

			/*let sum=0
			for(a=0;a<subtotals.length;a++){
				console.log(subtotals[a].subtotal)
				sum += subtotals[a].subtotal
				
			cart.cartTotal=sum

			// console.log(order[i].totalAmount)
			}
			// console.log(sum)
			// cart[i].save()
		
		return cart[0]*/
	})
	// console.log(isOrderUpdated)		
		
				return isCartUpdated
			
}

// get cart
module.exports.getCart=(data)=>{
	return userCart=Cart.findOne({orderdBy:data.userId}).then(cart=>{
		
			if (cart==null || cart==' ' ||cart==undefined){
				return `You don't have any orders yet!`
			} else {
				return cart
			}
			

		})
		

}

// create order
module.exports.createOrder= async (data)=>{

	// let amount= await Product.findById(data.productId).then( (p,error)=>{
	// 		if (error){
	// 			return false
	// 		} else{
	// 			return price=p.price
	// 		}

	// 		})

	/*let isProductActive = await Product.findById(data.productId).then(result=>{
		return result.isActive
	})

	let productName = await Product.findById(data.productId).then(result=>{
		return result.name
	})
*/	

	let orderItems= await Cart.findOne({orderdBy:data.userId}).exec()

	let order= await Order.findOne({orderdBy:data.userId}).then(order=>{
		if(orderItems.products.length>0){
		let newOrder= new Order({
					
					products:orderItems.products,
					totalAmount:orderItems.cartTotal,
					orderdBy:data.userId
				})
				
			 return newOrder.save().then((newOrder,error)=>{
					if (error){
						return false
					} else{
						return newOrder
					}
				})
			
	}

})
	return order
}

module.exports.removeFromCart=(data)=>{
	let isCartUpdated= Cart.findOne({orderdBy:data.userId}).then(cart=>{
		let test=cart.products.find(id=>id.productId==data.productId)
		if(test !==undefined){
		let index = cart.products.findIndex(i=>i.productId==data.productId)
		cart.products.splice(index,1)
		} else{
			return
		}

	
		let sum = 0
			for(let i=0;i<cart.products.length;i++){
					sum+=cart.products[i].subtotal
			}
			cart.cartTotal=sum
		// console.log(sum)
		cart.save()
		
	})

	let isUserUpdated=  User.findById(data.userId).then(user=>{
		let test=user.cart.find(id=>id.productId==data.productId)
		let index = user.cart.findIndex(i=>i.productId==data.productId)
		
		
			if(test==undefined){
				return false
			} else{
				
					user.cart.splice(index,1)
				
				return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return true
					}
				})
			}		
	})
	return cart
	
}